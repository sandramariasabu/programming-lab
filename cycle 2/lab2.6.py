# Program to perform List comprehensions:
# (a) Generate positive list of numbers from a given list of integers
# (b) Square of N numbers
# (c) Form a list of vowels selected from a given word
# (d) Form a list ordinal value of each element of a word (Hint: use ord() to get ordinal values)

#to generate positive list of numbers from a given list of integers
print("Enter a list of numbers(seperated by space): ")
list1 = [int(i) for i in input().split()] # accepts the list of numbers from the user and splits them into a list.
positive = [i for i in list1 if i >= 0]
print("Positive numbers in the list: ", positive)
  
#square of N numbers
print("Enter a list of numbers (seperated by space): ")
list2 = [int(i) for i in input().split()] # accepts the list of numbers from the user and splits them into a list.
square =[i**2 for i in list2] # returns the square of each element of the list.
print("Squares of the numbers in list: ", square)

#to generate vowels list from a given word
word1 =input("Enter a word: ") # accepts a word from user
vowels =[i for i in word1 if i in "a,e,i,o,u,A,E,I,O,U"] # checks if the given word is a vowel or not and if yes,returns it.
print("list of vowels in the word: ", vowels)

#to generate ordinal value of each element of a word
word2= input("Enter a word: ") # accepts a word from user
ordinal =[ord(i) for i in word2] # returns the ordinal value of each element of the word.
print("list of ordinal value of each element of the word : ", ordinal)
