"""program to read two lists color-list1 and color-list2 . Print out all colors from
color-list1 not contained in color-list2"""

print("Enter the colors in first list(seperated by spaces): ")
colorlist1=input().split()
print("Enter the colors in second list(seperated by spaces): ")
colorlist2=input().split()
print("colors in color-list1 not contained in color-list2 are:")
for i in colorlist1:
	if i not in colorlist2:
		print(i,end=" ")
print()
