"""program that accepts a string from the user and redisplays the string after removing
vowels from it"""

print("Enter the string:")
text=input()
vowels=['a','e','i','o','u','A','E','I','O','U']
newtext=""
textlen=len(text)
for i in range(textlen):
	if text[i] not in vowels:
		newtext=newtext+text[i]
print("STRING AFTER REMOVING VOWELS:")
text=newtext
print(text)

