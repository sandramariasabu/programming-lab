"""program to prompt the user to enter two lists of integers and check
(a) Whether lists are of the same length.
(b) Whether the list sums to the same value .
(c) Whether any value occurs in both Lists."""

print("Enter the elements in first list(seperated with commas): ")
list1=[int (x) for x in input().split(",")]
print("Enter the elements in second list(seperated with commas): ")
list2=[int (x) for x in input().split(",")]
if len(list1)==len(list2):
	print("lists are of same length")
else:
	print("lists are not of same length")

if sum(list1)==sum(list2):
	print("lists sums to the same value")
else:
	print("lists donot sums to the same value")

intersection_set=set.intersection(set(list1),set(list2))
intersection_list=list(intersection_set)
print("Values in both list are:",intersection_list)
