class rect:
    def __init__(self,l,b):
        self.a1=l
        self.a2=b

    def area(self):
        self.m=self.a1*self.a2

    def peri(self):
        self.n=2*(self.a1 + self.a2)

    def disp(self):
        print("Area of rectangle:", self.m)
        print("Perimeter of rectangle:", self.n)

    def compare(self,obj2):
        if self.m == obj2.m:
            print("Areas are equal")
        elif self.m > obj2.m:
            print("Area of 1st rectangle is greater than Area of 2nd rectangle")
        else:
            print("Area of 2nd rectangle is greater than Area of 1st rectangle")

l1=int(input("length of 1st rectangle:"))
b1=int(input("breadth of 1st rectangle:"))
obj1=rect(l1,b1)
obj1.area()
obj1.peri()
obj1.disp()
l2=int(input("length of 2nd rectangle:"))
b2=int(input("breadth of 2nd rectangle:"))
obj2=rect(l2,b2)
obj2.area()
obj2.peri()
obj2.disp()

obj1.compare(obj2)



