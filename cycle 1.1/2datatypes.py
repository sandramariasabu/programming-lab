"""program to demonstrate different number data types in python"""

a=2
b=6.7
c=6+2j
print(a,"is of type",type(a))
print(b,"is of type",type(b))
print(c,"is of type",type(c))