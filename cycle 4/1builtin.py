"""built-in packages (date and time Modules, Calendar, datetime, math
Module, string module)"""


from datetime import date
print("DATE: ",date.today())   #date module

import time
print("TIME: ",time.time())        #time module

import datetime
a=datetime.datetime.now()  #date time module
print("DATE & TIME: ",a)

import calendar
print(calendar.calendar(2022)) #calendar module

import math
n=int(input("Enter a number: "))                       #math module
print("square root of",n,"is: ",math.sqrt(n))
print("sine value of",n,"is: ",math.sin(n))
print("tangent value of",n,"is: ",math.tan(n))
print("cosine value of",n,"is: ",math.cos(n))
print("")

from math import pi
n=int(input("Enter the radius of circle:"))
print("area of a circle with radius",n,"is: ",pi*n*n) # import value of pi from math module
print("")

import string
print("ENGLISH ALPHABETS: ",string.ascii_letters)       #string modules
print("lowercase:  ",string.ascii_lowercase)
print("UPPERCASE:  ",string.ascii_uppercase)
print("Numerals:  ",string.digits)
print("Whitespaces:  ",string.whitespace)

