"""program to generate Fibonacci series of N terms"""

n=int(input("NUMBER OF TERMS: "))
a=0
b=1
count=0
if(n<=0):
	print("ENTER A POSITIVE INTEGER")
else:
	print("FIBONACCI SERIES: ")
	while(count<n):
		print(a)
		temp=a+b
		a=b
		b=temp
		count=count+1
